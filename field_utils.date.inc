<?php

/**
 * @file
 * Functions for date related fields.
 */

/**
 * Form builder function for Date fields.
 */
function field_utils_date_settings() {
  module_load_include('inc', 'field_utils');

  $form['field_utils_fields'] = array(
    '#theme' => 'field_utils_list_fields',
    '#tree' => TRUE,
  );
  $form['field_utils_fields']['field_utils_fields_options'] = array();
  $field_types = _field_utils_available_date_types();
  $fields = field_utils_get_fields($field_types);
  foreach ($fields as $field_name => $field_type) {
    $field_types_local = $field_types;
    $elem = array();
    $elem['field_name'] = array(
      '#markup' => $field_name,
    );

    $elem['field_type'] = array(
      '#markup' => $field_type,
    );

    // Remove the current type from the list of types which field can be
    // changed in to.
    if (isset($field_types_local[$field_type])) {
      unset($field_types_local[$field_type]);
    }

    $elem['field_types'] = array(
      '#type' => 'select',
      '#default_value' => '',
      '#options' => $field_types_local,
    );

    $form['field_utils_fields']['field_utils_fields_options'][$field_name] = $elem;
  }

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Convert'),
  );
  return $form;
}

/**
 * Submission function for field_utils_date_settings form.
 */
function field_utils_date_settings_submit($form, &$form_state) {

  if (isset($form_state['values']['field_utils_fields']['field_utils_fields_options'])) {
    // Load existing data.
    foreach ($form_state['values']['field_utils_fields']['field_utils_fields_options'] as $field_name => $data) {
      if ($data['field_types'] == 'none') {
        continue;
      }
      $field_info = field_info_field($field_name);
      if ($field_info['type'] != $data['field_types']) {
        if (field_utils_date_transform($field_name, $data['field_types']) === TRUE) {
          drupal_set_message(_field_utils_set_message($field_name, $field_info['type'], $data['field_types'], FALSE), 'status');
        }
        else {
          drupal_set_message(_field_utils_set_message($field_name, NULL, NULL, TRUE), 'error');
        }
      }
    }
  }
}

/**
 * Helper function to list all the supported date types.
 */
function _field_utils_available_date_types() {
  $types = array(
    'none' => t('- No change -'),
  );
  $types += array(
    'datetime' => 'datetime',
    'datestamp' => 'datestamp',
  );

  return $types;
}

/**
 * Transforms field to the desired type.
 *
 * @param string $field_name
 *   Name of the field which needs to be transformed.
 * @param string $type
 *   Name of the type of the field which the $field_name will be transformed
 *   into.
 *
 * @return bool
 *   Returns TRUE if all goes as planned.
 */
function field_utils_date_transform($field_name, $type) {
  // Field name in which values are stored.
  $field_name_value = $field_name . '_value';
  // Results of the table.
  $table_results = array();
  // Each field has also a revision.
  $field_versions = array('data', 'revision');
  foreach ($field_versions as $field_version) {
    $field_table_name = 'field_' . $field_version . '_' . $field_name;
    $field_table_backup_name = _field_utils_get_temp_table_name($field_table_name);

    // Saving values from the table.
    $table_results[$field_version] = db_select($field_table_name, 'c')
      ->fields('c')
      ->execute()
      ->fetchAll();

    // Saving a schema of the table.
    $schema = drupal_get_schema($field_table_name, TRUE);
    $db_field_db_name = 'type';
    $db_field_db_val = $type;
    // Depending on the type, DB schema needs different values.
    // Those values are mapped here.
    switch ($type) {
      case 'datetime':
        // TODO: does this work if DB is not mysql? Does the API fill in
        // other needed keys/values to make it work on non-mysql?
        $db_field_db_name = 'mysql_type';
        break;

      case 'datestamp':
        $db_field_db_val = 'int';
        break;
    }

    // Removing field setting here.
    unset($schema['fields'][$field_name_value]);
    // And replacing it with new one.
    $schema['fields'][$field_name_value][$db_field_db_name] = $db_field_db_val;

    // Dropping a backup table if it exists.
    db_drop_table($field_table_backup_name);
    // Creating a backup table from the current table.
    $query = "CREATE TABLE {" . check_plain($field_table_backup_name) . "} AS SELECT * FROM {" . check_plain($field_table_name) . "}";
    db_query($query);
    // Dropping current table.
    db_drop_table($field_table_name);
    // And creating a table with a new Schema.
    db_create_table($field_table_name, $schema);
  }
  // Updating date field type. This cannot be done with field_update_field()
  // function. Once the field is created, it cannot be changed via code.
  // DB query is the only way.
  db_update("field_config")
    ->fields(array(
      'type' => $type,
    ))
    ->condition('field_name', $field_name, '=')
    ->execute();

  // Since field_config table has been changed directly, cache has to be
  // cleared before getting info about the field.
  field_info_cache_clear();
  $field = field_info_field($field_name);
  // Changing some values in order to make things work.
  $field['settings']['granularity'] = [
    'month' => 'month',
    'day' => 'day',
    'hour' => 'hour',
    'minute' => 'minute',
    'second' => 'second',
    'year' => 'year',
  ];
  $field['settings']['tz_handling'] = 'site';
  $field['settings']['timezone_db'] = 'UTC';
  // Removing columns, let them be set to default.
  unset($field['columns']);
  // Saving new field settings.
  field_update_field($field);

  // Copying old data back to the original tables.
  foreach ($table_results as $field_version => $result) {
    foreach ($result as $r) {
      $fields_insert = (array) $r;
      $field_table_name = 'field_' . $field_version . '_' . $field_name;

      switch ($type) {
        case 'datetime':
          $value = date('Y-m-d H:i:s', $fields_insert[$field_name_value]);
          break;

        case 'datestamp':
          $value = strtotime($fields_insert[$field_name_value]) + 3600;
          break;
      }
      $fields_insert[$field_name_value] = $value;

      db_insert($field_table_name)
        ->fields($fields_insert)
        ->execute();
    }
  }
  return TRUE;
}
