<?php

/**
 * @file
 * Drush functions.
 */

/**
 * Implements hook_drush_command().
 */
function field_utils_drush_command() {

  $items['field_utils_transform'] = array(
    'description' => 'Convert field types.',
    'aliases' => array('fut'),
    'arguments' => array(
      'field_name' => 'Name of the field which you want to convert.',
      'field_type_to' => 'Name of the field type in which you want to change it.',
    ),
  );

  return $items;
}

/**
 * Calls a function which transforms field to the desired type.
 *
 * @param string $field_name
 *   Name of the field which needs to be transformed.
 * @param string $field_type_to
 *   Name of the type of the field which the $field_name will be transformed
 *   into.
 */
function drush_field_utils_transform($field_name, $field_type_to) {
  $field_info = field_info_field($field_name);
  switch (field_utils_get_type_of_field($field_name)) {
    case 'date':
      if ($field_info['type'] == $field_type_to) {
        return drush_log(t('%field is already a type of %type',
          array(
            '%field' => $field_name,
            '%type' => $field_type_to,
          )), 'error');
      }
      module_load_include('inc', 'field_utils', 'field_utils.date');
      if (field_utils_date_transform($field_name, $field_type_to) === TRUE) {
        drush_log(_field_utils_set_message($field_name, $field_info['type'], $field_type_to), 'ok');
      }
      else {
        drush_log(_field_utils_set_message($field_name, NULL, NULL, TRUE), 'error');
      }
      break;

    case 'file':
      if ($field_info['settings']['uri_scheme'] == $field_type_to) {
        return drush_log(t('%field is already a %type file',
          array(
            '%field' => $field_name,
            '%type' => $field_type_to,
          )), 'error');
      }
      module_load_include('inc', 'field_utils', 'field_utils.file');
      if (field_utils_file_transform($field_name, $field_type_to) === TRUE) {
        drush_log(_field_utils_set_message($field_name, $field_info['settings']['uri_scheme'], $field_type_to), 'ok');
      }
      else {
        drush_log(_field_utils_set_message($field_name, NULL, NULL, TRUE), 'error');
      }
      break;
  }
}
