<?php

/**
 * @file
 * Functions for file related fields.
 */

/**
 * Form builder function for File fields.
 */
function field_utils_file_settings() {
  module_load_include('inc', 'field_utils');

  $form['field_utils_fields'] = array(
    '#theme' => 'field_utils_list_fields',
    '#tree' => TRUE,
  );
  $form['field_utils_fields']['field_utils_fields_options'] = array();
  $field_types = _field_utils_available_file_types();
  $fields = field_utils_get_fields(array('file'));
  foreach ($fields as $field_name => $field_type) {
    $field_info = field_info_field($field_name);

    $field_types_local = $field_types;
    $elem = array();
    $elem['field_name'] = array(
      '#markup' => $field_name,
    );

    // Indicate whether the file field is public or private.
    $elem['field_type'] = array(
      '#markup' => $field_info['settings']['uri_scheme'] . ' ' . $field_type,
    );

    // Remove the current type from the list of types which field can be
    // changed in to.
    if (isset($field_types_local[$field_info['settings']['uri_scheme']])) {
      unset($field_types_local[$field_info['settings']['uri_scheme']]);
    }

    $elem['field_types'] = array(
      '#type' => 'select',
      '#default_value' => '',
      '#options' => $field_types_local,
    );

    $form['field_utils_fields']['field_utils_fields_options'][$field_name] = $elem;
  }

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Convert'),
  );
  return $form;
}

/**
 * Submission function for field_utils_file_settings form.
 */
function field_utils_file_settings_submit($form, &$form_state) {

  if (isset($form_state['values']['field_utils_fields']['field_utils_fields_options'])) {
    // Load existing data.
    foreach ($form_state['values']['field_utils_fields']['field_utils_fields_options'] as $field_name => $data) {
      if ($data['field_types'] == 'none') {
        continue;
      }
      $field_info = field_info_field($field_name);
      if ($field_info['settings']['uri_scheme'] != $data['field_types']) {
        if (field_utils_file_transform($field_name, $data['field_types']) === TRUE) {
          drupal_set_message(_field_utils_set_message($field_name, $field_info['settings']['uri_scheme'], $data['field_types'], FALSE), 'status');
        }
        else {
          drupal_set_message(_field_utils_set_message($field_name, NULL, NULL, TRUE), 'error');
        }
      }
    }
  }
}

/**
 * Helper function to list file types.
 */
function _field_utils_available_file_types() {
  $types = array(
    'none' => t('- No change -'),
  );
  $types += array(
    'private' => 'private',
    'public' => 'public',
  );

  return $types;
}

/**
 * Transforms field to the desired type.
 *
 * @param string $field_name
 *   Name of the field which needs to be transformed.
 * @param string $type
 *   Name of the type of the field which the $field_name will be transformed
 *   into.
 *
 * @return bool
 *   Returns TRUE if all goes as planned.
 */
function field_utils_file_transform($field_name, $type) {
  // Each field has also a revision.
  $field_versions = array('data', 'revision');
  foreach ($field_versions as $field_version) {
    // Table name which is compiled from few variables.
    $field_table_name = 'field_' . $field_version . '_' . $field_name;
    // Backup table name, static suffix is added.
    $field_table_backup_name = _field_utils_get_temp_table_name($field_table_name);

    // Dropping a backup table if it exists.
    db_drop_table($field_table_backup_name);
    // Backup current table and its values.
    $query = "CREATE TABLE {" . check_plain($field_table_backup_name) . "} AS SELECT * FROM {" . check_plain($field_table_name) . "}";
    db_query($query);
    // Empty current table, so field type can be changed.
    db_truncate($field_table_name)->execute();
  }
  // Changing file field to private/public.
  $field = field_info_field($field_name);
  $field['settings']['uri_scheme'] = $type;
  field_update_field($field);

  // Copying data back from backup table to the original one.
  foreach ($field_versions as $field_version) {
    $field_table_name = 'field_' . $field_version . '_' . $field_name;
    $field_table_backup_name = _field_utils_get_temp_table_name($field_table_name);
    $query = db_select($field_table_backup_name, 'b')
      ->fields('b');

    db_insert($field_table_name)
      ->from($query)
      ->execute();
  }
  return TRUE;
}
