Have you ever found yourself in a situation when you had to change field types
but Drupal did not let you do this because you already had values in the DB?
And than you had to write not so arbitrary code to change field settings
in the database. This module helps you do this change with a simple UI
or drush command.

At the moment, module can transform:

File fields from private to public and vice versa. Date fields from/to
Datestamp and Datetime (dates are of course transformed into the right form)
by keeping all the values.

For example, if you want to make your files private from public,
just run

drush fut field_name private

Module makes a backup of the field values table first, so if anything
goes wrong, you can revert it back manually. Backup tables are named
the same as the original one with an added suffix '_field_utils_bak'.
Please note that backup tables are removed when CRON is executed.
